<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * CorreosForm is the model behind the contact form.
 */
class CorreosForm extends Model
{
    public $Nombre;
    public $Email;
    public $Titulo;
    public $Mensaje;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['Nombre', 'Email', 'Titulo', 'Mensaje'], 'required'],
            ['Email', 'email'],
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function enviar()
    {
        $data = Yii::$app->request->post()["CorreosForm"];
        $this->load($data);
        if ($this->validate()) {
            $message =
              "Se ha enviado un correo, los datos del formulario son los siguientes:<br>
                Nombre: ".$data['Nombre']."<br>
                Email: ".$data['Email']."<br>
                Mensaje: ".$data['Mensaje']."<br>";
            Yii::$app->mailer->compose()
               ->setFrom('lcampos@solmipro.com')
               ->setTo('lcampos@solmipro.com')
               ->setBcc(['luisecbustos@gmail.com', 'ccastaneira@tecnocen.com'])
               ->setSubject($data["Titulo"])
               ->setHtmlBody($message)
               ->send();
            return true;
        }
        return false;
    }
}
