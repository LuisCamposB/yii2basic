<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\CorreosForm;

class CorreosController extends Controller
{

    public $layout = "correos";

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEnviar()
    {
        $model = new CorreosForm();
        if ($model->load(Yii::$app->request->post())) {
            $this->view->params["result"]= $model->enviar();
            Yii::$app->session->setFlash('correosFormSubmitted');
        }
        return $this->render('enviar', [
          'model' => $model,
      ]);
    }
}
