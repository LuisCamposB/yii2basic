<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Enviar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-enviar">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('correosFormSubmitted')): ?>
      <?php if ($this->params["result"]): ?>
        <div class="alert alert-success">
            El Correo ha sido enviado
        </div>
      <?php else: ?>
        <div class="alert alert-danger">
            El Correo no se pudo enviar, por favor, intente de nuevo
        </div>
      <?php endif; ?>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'Nombre')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'Email') ?>

                    <?= $form->field($model, 'Titulo') ?>

                    <?= $form->field($model, 'Mensaje')->textArea(['rows' => 6]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Enviar Correo', ['class' => 'btn btn-primary', 'name' => 'correos-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
